README file for PHYM004 final project on the chaotic behaviour of a dripping tap.
Written by Harrison Nicholls (hen204@exeter.ac.uk)

## Overview
This code follows the method in [1] and [2] to simulate the behaviour of a dripping tap. Most of the integration 
is done using the "shm" system of equations; time t is the independent variable, and position x, velocity v, and 
mass m are dependent variables. These describe the properties of the "residue" of fluid attached to the tap, 
which grows in x and m until it reaches a critical length xc. At x=xc, the residue should drip: releasing a mass 
dm and shrinking back to some x=x0. The main difficulty comes from the fact that integration is discrete, and 
the residue will always overshoot xc by some amount. This means that I must integrate back to xc using the method
described in [2].

## Parameters and configuration
 * a    Scale factor for droplet mass   [s/cm   ]
 * R    Flow rate through tap           [g.s-1  ]
 * k    Spring constant of residue      [dy.cm-1]
 * b    Damping constant of residue     [s-1    ]
 * g    Gravitational acceleration      [cm.s-2 ]
 * d    Fluid density                   [g.cm-3 ]
 * xc   Critical distance               [cm     ]
 * m0   Initial residue mass            [g      ]
 * x0   Initial residue position        [cm     ]
 * v0   Initial residue velocity        [cm.s-1 ]


## References
[1]: D'Innocenzo, A., Renna, L; Dripping faucet. Int J Theor Phys 35, 941–973 (1996). https://doi.org/10.1007/BF02302382
[2]: M. Henon; On the numerical computation of Poincaré maps, Physica D: Nonlinear Phenomena, Volume 5, Issues 2–3, 1982, Pages 412-414, https://doi.org/10.1016/0167-2789(82)90034-3.
