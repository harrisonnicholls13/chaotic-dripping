FILE* fileio_open(char* path);
void fileio_writeIter(FILE* file, int i, double t, double dt, double x, double v, double m, bool dripped);
void fileio_writeParam(FILE* file, void* params);
void fileio_close(FILE* file, int status);
