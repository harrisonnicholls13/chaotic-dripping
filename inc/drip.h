#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>
#include <limits.h>

// Prefixes to print messages (with colors!)
#define PRT_SPACE		    "---------  "
#define PRT_MSG			    "\033[0;32m[Message]  \033[0m"  // General message
#define PRT_PRMPT		    "\033[0;36m[Command]  \033[0m"  // Prompt
#define PRT_QUERY		    "\033[0;34m[ Query ]  \033[0m"  // Query for input
#define PRT_WARN		    "\033[0;33m[Warning]  \033[0m"  // Warning 
#define PRT_ERROR		    "\033[0;31m[ Error ]  \033[0m"  // Error
#define STRLEN              128

// Macros for neater code
#define UNUSED(_x)                ( (void)(_x) )
#define STREQ(_s,_x)              ( strcmp(_s,_x) == 0 ? 1 : 0)      // Checks if strings are equal
#define EVEN(_xv)                 ( ((_xv) % 2) == 0 ? 1 : 0)        // Check if x is even
#define DBL_PCT_CNG(_fnl,_ini)    ( fabs( ((_fnl) - (_ini)) / (_ini) * 100 ))   // Calculate percentage change in a value (for doubles)

// Constants and units
#define DBL_BIG     (1e300)         // Big value for a double
#define DBL_SML     (1e-300)        // Small value for a double
#define pi_c        (3.14159265)    // Pi

// Define guard
#ifndef GUARD
#define GUARD

#include "solver.h"
#include "util.h"
#include "fileio.h"

#endif
