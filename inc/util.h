// Prototype functions....
double      frand(double min, double max);
double      getTime(void);
double**    malloc2D(int n1, int n2);
void        free2D(double** v, int n1);
void        copy2D(double** src, double** dst, int n1, int n2);
double mean(double* arr, int N);
