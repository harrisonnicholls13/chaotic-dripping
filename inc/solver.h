#include <gsl/gsl_errno.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_odeiv2.h>

// Solver return codes
#define SOLVE_RET_SUC (0)
#define SOLVE_RET_ERR (1)
#define SOLVE_RET_DRP (2)
#define SOLVE_RET_FIN (3)

// Compile-time arguments
#define DRP_MODE      (1)

bool solver_run(double dt, double tf, int iter_max,
                double x0, double v0, double m0, void* params, 
                const gsl_odeiv2_step_type* stepper, int gsl_n, 
                FILE* fstream, int mod_prt, int mod_wrt, bool prt_drp, bool wrt_drp);