#include "drip.h"

/**
 * System of equations for main integration (shm part)
 * @param t         Current time
 * @param y         Generalised coordinates (x,v,m)
 * @param f         Equations of motion 
 * @param params    Parameters and constants system
 */
int _shm_func(double t, const double y[], double f[], void *params) {
    double R   =  ((double *)params)[1];
    double k   =  ((double *)params)[2];
    double b   =  ((double *)params)[3];
    double g   =  ((double *)params)[4];

    UNUSED(t);

    // Note that:
    // y[0] = y0 = x
    // y[1] = y1 = v
    // y[2] = y2 = m

    f[0] = y[1];                               // d/dt(x)
    f[1] = g - ( (k*y[0]) + (b+R)*y[1])/y[2];  // d/dt(v)     
    f[2] = R;                                  // d/dt(m)
    return GSL_SUCCESS;
}


/**
 * Derivatives required for main integration (shm part)
 * @param t         Current time
 * @param y         Generalised coordinates (x,v,m)
 * @param dfdy      Jacobian matrix
 * @param dfdt      Time derivatives of EOM
 * @param params    Parameters and constants system
 */
int _shm_jac(double t, const double y[], double *dfdy, double dfdt[], void *params) {
    double R  =  ((double *)params)[1];
    double k  =  ((double *)params)[2];
    double b  =  ((double *)params)[3];

    UNUSED(t);

    // A matrix view is a temporary object, stored on the stack, which can be used to operate on a subset of matrix elements
    gsl_matrix_view dfdy_mat = gsl_matrix_view_array(dfdy, 3, 3);
    gsl_matrix *mat = &dfdy_mat.matrix;

    // Access matrix elements and set values of jacobian
    // row 1
    gsl_matrix_set(mat, 0, 0, 0.0);
    gsl_matrix_set(mat, 0, 1, 1.0);
    gsl_matrix_set(mat, 0, 2, 0.0);
    // row 2
    gsl_matrix_set(mat, 1, 0, (-1.0) * k / y[2]);
    gsl_matrix_set(mat, 1, 1, (-1.0) * (b+R) / y[2]); 
    gsl_matrix_set(mat, 1, 2, ( (k*y[0]) + (b+R)*y[1]) / (y[2]*y[2]) );
    // row 3
    gsl_matrix_set(mat, 2, 0, 0.0);
    gsl_matrix_set(mat, 2, 1, 0.0);
    gsl_matrix_set(mat, 2, 2, 0.0);

    // Vector of partial derivative elements
    dfdt[0] = 0.0;  // d/dt(f0)
    dfdt[1] = 0.0;  // d/dt(f1)
    dfdt[2] = 0.0;  // d/dt(f2)
    return GSL_SUCCESS;
}


/**
 * System of equations for Henon integration
 * @param x         Independent coordinate (position x)
 * @param y         Generalised coordinates (t,v,m)
 * @param f         Equations of motion 
 * @param params    Parameters and constants system
 */
int _hen_func(double x, const double y[], double f[], void *params) {
    double R  =  ((double *)params)[1];
    double k  =  ((double *)params)[2];
    double b  =  ((double *)params)[3];
    double g  =  ((double *)params)[4];

    // Note that:
    // y[0] = y0 = t
    // y[1] = y1 = v
    // y[2] = y2 = m

    f[0] = 1/y[1];                                    // d/dx(t)
    f[1] = g/y[1] - (k*x)/(y[1]*y[2]) - (b+R)/y[2];   // d/dx(v)
    f[2] = R/y[1];                                    // d/dx(m)
    return GSL_SUCCESS;
}

/**
 * Derivatives required for Henon integration
 * @param x         Independent coordinate (position x)
 * @param y         Generalised coordinates (t,v,m)
 * @param dfdy      Jacobian matrix
 * @param dfdt      x derivatives of EOM
 * @param params    Parameters and constants system
 */
int _hen_jac(double x, const double y[], double *dfdy, double dfdt[], void *params) {
    double R  =  ((double *)params)[1];
    double k  =  ((double *)params)[2];
    double b  =  ((double *)params)[3];
    double g  =  ((double *)params)[4];

    // A matrix view is a temporary object, stored on the stack, which can be used to operate on a subset of matrix elements
    gsl_matrix_view dfdy_mat = gsl_matrix_view_array(dfdy, 3, 3);
    gsl_matrix *mat = &dfdy_mat.matrix;

    // Access matrix elements and set values of jacobian
    // row 1
    gsl_matrix_set(mat, 0, 0, 0.0);
    gsl_matrix_set(mat, 0, 1, -1.0 / (y[1]*y[1]));
    gsl_matrix_set(mat, 0, 2, 0.0);
    // row 2
    gsl_matrix_set(mat, 1, 0, 0.0);
    gsl_matrix_set(mat, 1, 1, (k*x/y[2] - g)/(y[1]*y[1])); 
    gsl_matrix_set(mat, 1, 2, (k*x/y[1] + b+R)/(y[2]*y[2]));
    // row 3
    gsl_matrix_set(mat, 2, 0, 0.0);
    gsl_matrix_set(mat, 2, 1, -1.0*(b+R) / (y[1]*y[1]));
    gsl_matrix_set(mat, 2, 2, 0.0);

    // Vector of partial derivative elements
    dfdt[0] = 0.0;                        // d/dx(f0)
    dfdt[1] = (-1.0) * k / (y[2] * y[1]);   // d/dx(f1)
    dfdt[2] = 0.0;                        // d/dx(f2)
    return GSL_SUCCESS;
}


int _solver_shm(int* iter, double *t, double dt, double tf, int iter_max,
                double* y, void* params, int ndrip, double mOut, 
                const gsl_odeiv2_step_type* stepper,
                FILE* fstream, int mod_prt, int mod_wrt, bool prt_drp) {


    // Set up solver for main simulation (shm)
    gsl_odeiv2_system   shm_sys = {_shm_func, _shm_jac, 3, params};
    gsl_odeiv2_driver*  shm_drv = gsl_odeiv2_driver_alloc_y_new(&shm_sys, stepper, 1e-7, 1e-6, 0.0);

    // Parameters of system
    double R  =   ((double *)params)[1];
    double xc =   ((double *)params)[5];

    // Variables of system
    double  tn =            *t;             // Next time
    int     statusL =       -1;             // Did gsl driver work this iteration?
    bool    dripped =       false;          // Dripped this iteration?
    double  mE =            0.0;            // Mass flow error
    double  mF =            0.0;            // Total mass flow

    // Try and integrate until we reach the final time
    while (*t < tf) {

        if (*iter > iter_max) {
            printf(PRT_WARN"Maximum iterations reached \n");
            return SOLVE_RET_FIN;
        }

        // Get ready for new iteration
        *iter = *iter + 1;
        *t = tn;
        tn = *t + dt;

        // This function evolves the driver system d from t to t1. Initially vector y should contain the values of dependent variables at point t
        statusL = gsl_odeiv2_driver_apply(shm_drv, t, tn, y);
        // statusL = gsl_odeiv2_driver_apply_fixed_step(shm_drv, t, dt/200, 200, y);
        if (statusL != GSL_SUCCESS) {
            printf(PRT_ERROR"Problem iterating shm_drv. GSL error message: '%s'\n",  gsl_strerror(statusL));
            return SOLVE_RET_ERR;
        }

        // Calculate mass flow error
        mF = R*(*t); 
        mE = DBL_PCT_CNG(mOut, mF);

        // Dripped?
        dripped = (y[0] > xc);

        // Print state of system
        if ( (*iter%mod_prt == 0) || (dripped && prt_drp) ) {
            printf(PRT_MSG"************* Iteration %5d ************ \n",*iter);
            printf(PRT_MSG"t  =  %3.3e s        m  =  %3.3e g     \n",*t,y[2]);
            printf(PRT_MSG"x  = %+3.3e cm       v  = %+3.3e cm/s  \n",y[0], y[1]);
            printf(PRT_MSG"mD = %+3.3e g.cm/s   mE = %3.3e%%     \n",mOut-mF,mE);
            printf(PRT_MSG"Nd = %07d             d? = %s \n", ndrip, (dripped?"true":"false"));
            printf(PRT_MSG"\n");
        }

        // Handle drip if it occurred
        if (dripped) {
            gsl_odeiv2_driver_free(shm_drv);
            return SOLVE_RET_DRP;
        } else {
            // Write state of system to file
            if (*iter%mod_wrt == 0) {
                fileio_writeIter(fstream, *iter, *t, dt, y[0], y[1], y[2], false);
            }
        }
    }
    gsl_odeiv2_driver_free(shm_drv);
    return SOLVE_RET_FIN;
}

int _solver_hen(int* iter, double *t, 
                double* y, void* params, double* mOp,
                const gsl_odeiv2_step_type* stepper, int gsl_n, FILE* fstream, 
                bool prt_drp, bool wrt_drp) {

    
    // Parameters of system
    double  a =     ((double *)params)[0];
    double  xc =    ((double *)params)[5];
    double  d  =    ((double *)params)[6];
    double  mc =    0.0;
    double  vc =    0.0;
    
    // Set up solver for drip-reversal (hen)
    gsl_odeiv2_system   hen_sys = {_hen_func, _hen_jac, 3, params};
    gsl_odeiv2_driver*  hen_drv = gsl_odeiv2_driver_alloc_y_new(&hen_sys, stepper, 1e-7, 1e-6, 0.0);

    // Set up required variables
    int     statusL = SOLVE_RET_SUC;
    double  t_old   = *t;
    double  hen_x   = y[0];
    double  hen_t   = *t;
    double  hen_v   = y[1];
    double  hen_m   = y[2];
    double  dm      = 0.0; // change in mass
    double  xn      = 0.0; // new position

    // Henon method to bring x to xc
    double hen_y[] = {hen_t, hen_v, hen_m};
    if (prt_drp) printf(PRT_MSG"Henon integration start (t = %g, x = %g) \n", hen_y[0], hen_x);
    statusL = gsl_odeiv2_driver_apply_fixed_step (hen_drv, &hen_x, (xc-hen_x)/gsl_n, gsl_n, hen_y);
    if (statusL != GSL_SUCCESS) {
        printf(PRT_ERROR"Problem iterating hen_drv. GSL error message: '%s'\n",  gsl_strerror(statusL));
        return SOLVE_RET_ERR;
    } else {
        if (prt_drp) printf(PRT_MSG"Henon integration end   (t = %g, x = %g) \n", hen_y[0], xc);
    }

    mc = hen_y[2]; // Critical mass
    vc = hen_y[1]; // Critical velocity

    // Calculate change in mass due to drip
    switch (DRP_MODE) {
        case 1:
            dm = a*mc*vc; // Mode (i) from equation 3
            break;
        case 2:
             dm = a*vc; // Mode (ii) from equation 3
            break;
        default:
            printf(PRT_ERROR"Drip mode invalid \n");
            return SOLVE_RET_ERR;
    }
    *mOp += dm;

    // Calculate new position after drip using method in equation 4
    xn = xc - dm/mc*pow((3.0*dm)/(4.0*pi_c*d),1.0/3.0);

    // Set "real-space"  values at critical drip point
    y[0] = xn;
    y[1] = vc;
    y[2] = mc - dm;
    *t   = hen_y[0];

    // Print drip info
    // Mostly for debugging - this slows things down a lot
    if (prt_drp) {
        printf(PRT_MSG":::::::::::::::::: Drip :::::::::::::::::: \n");
        printf(PRT_MSG"t  =  %6.6f s  \t dm =  %6.6f g \n",*t,dm);
        printf(PRT_MSG"xn = %+6.6f cm \t vc = %+6.6f cm/s\n",y[0], y[1]);
        printf(PRT_MSG"\n");
    }

    // Write post-drip data
    if (wrt_drp) {
        fileio_writeIter(fstream, *iter, *t, (t_old - *t), y[0], y[1], y[2], true);
    }

    gsl_odeiv2_driver_free(hen_drv);
    return SOLVE_RET_SUC;
}


/**
 * Wrapper function around integrators - evolve system over time until t>=tf
 * @param dt        Integration time-step
 * @param tf        Final integration time
 * @param iter_max  Max iterations
 * @param x0        Initial x
 * @param v0        Initial v
 * @param m0        Initial m
 * @param params    Array of parameters and constants
 * @param stepper   Stepper algorithm used by GSL
 * @param gsl_n     Number of fixed steps used by Henon integrator
 * @param fstream   File stream
 * @param mod_prt   Frequency of prints to stdout
 * @param mod_wrt   Frequency of saving data to file
 * @param prt_drp   Print drip info?
 * @param wrt_drp   Always write drip iterations to file?
 */
bool solver_run(double dt, double tf, int iter_max,
                double x0, double v0, double m0, void* params, 
                const gsl_odeiv2_step_type* stepper, int gsl_n,
                FILE* fstream, int mod_prt, int mod_wrt, bool prt_drp, bool wrt_drp) {


    // Variables of system
    int     iter =          0;              // Iteration number
    bool    dripped =       false;          // Dripped this iteration?
    double  t =             0.0;            // Current time
    double  y[] =           {x0, v0, m0};   // Generalised coordinates for (shm)
    int     ndrip =         0;              // Drip count 
    double  mO =            0.0;            // Total dripped mass

    // Wrapper around the two solvers
    while (t < tf){

        // Call shm solver
        int status = 0;
        status = _solver_shm(&iter, &t, dt, tf, iter_max,
                             y, params, ndrip, mO,
                             stepper, 
                             fstream, mod_prt, mod_wrt, prt_drp);

        // Handle shm solver result
        dripped = false;
        switch(status) {
            case SOLVE_RET_SUC:
                // Solver ran but did nothing?
                break;
            case SOLVE_RET_ERR:
                // Solver error
                return SOLVE_RET_ERR;
            case SOLVE_RET_DRP:
                // Time to drip
                dripped = true; break;
            case SOLVE_RET_FIN:
                // Solver reached final time
                return SOLVE_RET_SUC;
            default:
                printf(PRT_ERROR"SHM return status invalid \n");
                return SOLVE_RET_ERR;
        }
       
        // Integrate using henon method if drip occurs, and handle other changes
        if (dripped) {
            int status = _solver_hen(&iter, &t, 
                                     y, params, &mO,
                                     stepper, gsl_n, fstream, 
                                     prt_drp, wrt_drp);
            dripped = false;
            if (status == SOLVE_RET_SUC) {
                // Drip completed successfully
            } else if (status == SOLVE_RET_ERR) {
                // Drip error
                return SOLVE_RET_ERR;
            } 
            ndrip++;
        }

        // Sanity check on mass
        if (y[2] < 0) {
            printf(PRT_WARN"Residue has negative mass \n");
            printf(PRT_WARN"x = %g \n",y[0]);
            printf(PRT_WARN"v = %g \n",y[1]);
            printf(PRT_WARN"m = %g \n",y[2]);
            return SOLVE_RET_ERR;
        }

        // Sanity check on variables
        for (int i = 0; i < 3; i++) {
            if (isnan(y[0]) || isinf(y[0])) {
                printf(PRT_WARN"Residue has non-finite %s \n",(i==0?"x":(i==1?"v":"m")));
                return SOLVE_RET_ERR;
            }
        }

    } // End of time loop
    return SOLVE_RET_SUC;
}
