#include "drip.h"

/**
 * Get random double in range min to max
 * @param min       Minimum of bound of range
 * @param max       Maximum of bound of range
 */
double frand( double min, double max ) {
    double scale = rand() / (double) RAND_MAX; 
    return min + scale * ( max - min );
}

/**
 * Allocate memory for a 2D array
 * @param n1        Length of first dimension
 * @param n2        Length of second dimension
 * @return          Pointer to array
 */
double** malloc2D(int n1, int n2) {
    double** v1 = (double**) malloc(n1*sizeof(double*));
    if (v1 == NULL) {
        printf(PRT_ERROR"Could not allocate memory \n");
        exit(EXIT_FAILURE);
    }
    for (int i = 0; i < n1; i++) {
        v1[i] = (double*) malloc(n2*sizeof(double));
        if (v1[i] == NULL) {
            printf(PRT_ERROR"Could not allocate memory \n");
            exit(EXIT_FAILURE);
        }
    }
    return v1;
}

/**
 * Free memory allocated to a 2D array
 * @param v         Pointer to first element of array
 * @param n1        Length of first dimension
 */
void free2D(double** v, int n1) {
    for (int i = 0; i < n1; i++) {
        free(v[i]);
    }
    free(v);
}


/**
 * Copy data between two 1D arrays (double)
 * @param src       Pointer to source
 * @param dst       Pointer to destination
 * @param n1        Length of first dimension
 */
void copy1D(double* src, double* dst, int n1) {
    for (int i = 0; i < n1; i++) {
        dst[i] = src[i];
    }
}

/**
 * Copy data between two 2D arrays (double)
 * @param src       Pointer to source
 * @param dst       Pointer to destination
 * @param n1        Length of first dimension
 * @param n1        Length of second dimension
 */
void copy2D(double** src, double** dst, int n1, int n2) {
    for (int i = 0; i < n1; i++) {
        for (int j = 0; j < n2; j++) {
            dst[i][j] = src[i][j];
        }
    }
}


/**
 * Calculate the mean of an array (double)
 * @param arr       Array of data
 * @param N         Length of array
 */
double mean(double* arr, int N) {
    double sum = 0;
    for (int i = 0; i < N; i++) {
        sum += arr[i];
    }
    return sum/((double) N);
}