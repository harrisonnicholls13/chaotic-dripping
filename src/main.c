#include "drip.h" 

int main(int argc, char* argv[]) {
    printf(PRT_MSG"Dripping tap simulation by Harrison Nicholls\n");

    // Set parameters -------------------------------------------------------------
    char    fout[STRLEN]=   "./res/out.dat";// Output file
    int     mod_prt =       1e2;            // Frequency at which to print to stdout
    int     mod_wrt =       5e2;            // Frequency at which to write data
    int     iter_max =      1e8;            // Maximum iterations
    bool    wrt_drp =       false;           // Always write drip iterations to a file?
    bool    prt_drp =       false;          // Print to stdout showing drip data?
    double  dt =            1e-2;           // Timestep
    double  tf =            1e2;            // Final time
    int     gsl_n =         50;             // Number of steps GSL uses for each dt
    double  prm_a =         0.25;           // Param: alpha
    double  prm_R =         0.90;           // Param: flow rate
    double  prm_k =         475;            // Param: spring constant
    double  prm_b =         1.0;            // Param: damping constant
    double  prm_g =         980;            // Param: gravity constant
    double  prm_xc =        0.19;           // Param: critical distance
    double  prm_d  =        1.0;            // Param: density
    double  m0 =            0.01;           // m0
    double  x0 =            0.0;            // x0
    double  v0 =            0.1;            // v0
    char*   solver =        "rk2";          // Solver to be used by GSL
    

    // Read run arguments ---------------------------------------------------------
    if (argc == 1) {
        // Do not change anything
        printf(PRT_MSG"No arguments provided \n");
    } else if (argc == 2){
        // Invalid
        printf(PRT_ERROR"Missing argument 3 \n");
        exit(EXIT_FAILURE);
    } else if (argc >= 3) {
        int args_n = argc-1;        // Number args that are in a pair
        char args_buf_k[STRLEN];    // Buffer for arg key
        char args_buf_v[STRLEN];    // Buffer for arg val
        if (EVEN(args_n)) {
            // Loop over parameter pairs
            for (int p = 1; p <= args_n; p+=2) {
                strcpy(args_buf_k,argv[p]);
                strcpy(args_buf_v,argv[p+1]);
                if (STREQ(args_buf_k,"a")) {
                    sscanf(args_buf_v, "%lf", &prm_a);
                } else if (STREQ(args_buf_k,"R")) {
                    sscanf(args_buf_v, "%lf", &prm_R);
                } else if (STREQ(args_buf_k,"k")) {
                    sscanf(args_buf_v, "%lf", &prm_k);
                } else if (STREQ(args_buf_k,"b")) {
                    sscanf(args_buf_v, "%lf", &prm_b);
                } else if (STREQ(args_buf_k,"g")) {
                    sscanf(args_buf_v, "%lf", &prm_g);
                } else if (STREQ(args_buf_k,"xc")) {
                    sscanf(args_buf_v, "%lf", &prm_xc);
                } else if (STREQ(args_buf_k,"d")) {
                    sscanf(args_buf_v, "%lf", &prm_d);
                } else if (STREQ(args_buf_k,"m0")) {
                    sscanf(args_buf_v, "%lf", &m0);
                } else if (STREQ(args_buf_k,"x0")) {
                    sscanf(args_buf_v, "%lf", &x0);
                } else if (STREQ(args_buf_k,"v0")) {
                    sscanf(args_buf_v, "%lf", &v0);
                } else if (STREQ(args_buf_k,"fout")) {
                    strcpy(fout, args_buf_v);
                } else {
                    printf(PRT_ERROR"Argument error: invalid key '%s' \n",args_buf_k);
                    exit(EXIT_FAILURE);
                }
            }
        } else {
            printf(PRT_ERROR"Argument error: uneven key/val pairings \n");
            exit(EXIT_FAILURE);
        }
    } 

    // Create variables ------------------------------------------------------------
    FILE* fstream = fileio_open(fout);
    const gsl_odeiv2_step_type* gsl_solver_step; // Stepper pointer
    if (STREQ(solver,"rk8")) {
        gsl_solver_step = gsl_odeiv2_step_rk8pd;
    } else if (STREQ(solver,"rk4")) {
        gsl_solver_step = gsl_odeiv2_step_rk4;
    } else if (STREQ(solver,"rkf45")) {
        gsl_solver_step = gsl_odeiv2_step_rkf45;
    } else if (STREQ(solver,"rk2")) {
        gsl_solver_step = gsl_odeiv2_step_rk2;
    } else if (STREQ(solver,"bdf")) {
        gsl_solver_step = gsl_odeiv2_step_msbdf;
    } else if (STREQ(solver,"bsm")) {
        gsl_solver_step = gsl_odeiv2_step_bsimp;
    } else {
        printf(PRT_ERROR"Unknown solver stepper selected \n");
        exit(EXIT_FAILURE);
    }
    int rcode = EXIT_SUCCESS;

    double R_mass = prm_R*prm_d; // Convert input flow rate from ml/s to g/s
    double params[] = {prm_a, R_mass, prm_k, prm_b, prm_g, prm_xc, prm_d}; // Collate parameters into array
    //                 0    , 1     , 2    , 3    , 4    , 5    , 6

    printf(PRT_MSG"System parameters: \n");
    printf(PRT_MSG"  a  = %4.4e cm.s-1 \n", prm_a);
    printf(PRT_MSG"  R  = %4.4e g.s-1  \n", prm_R);
    printf(PRT_MSG"  k  = %4.4e dy.cm-1\n", prm_k);
    printf(PRT_MSG"  b  = %4.4e s-1    \n", prm_b);
    printf(PRT_MSG"  g  = %4.4e cm.s-2 \n", prm_g);
    printf(PRT_MSG"  xc = %4.4e cm     \n", prm_xc);
    printf(PRT_MSG"  d  = %4.4e g.cm-3 \n", prm_d);
    printf(PRT_MSG"  m0 = %4.4e g      \n", m0);
    printf(PRT_MSG"  x0 = %4.4e cm     \n", x0);    
    printf(PRT_MSG"  v0 = %4.4e cm.s-1 \n", v0);


    // Solve system using GSL --------------------------------------------------------
    printf(PRT_MSG"\n");
    fileio_writeParam(fstream, &params);
    printf(PRT_MSG"Starting integration... \n");
    int solver_result = solver_run(dt,tf, iter_max, x0, v0, m0, &params, gsl_solver_step, gsl_n, fstream,mod_prt,mod_wrt, prt_drp, wrt_drp);

    if (solver_result == SOLVE_RET_ERR) {
        printf(PRT_WARN"********* Integration ended *********** \n");
        printf(PRT_WARN"    Solver encountered problems [%d]\n",solver_result);
        rcode = EXIT_FAILURE;
    } else {
        printf(PRT_MSG"******** Integration complete ********* \n");
    }

    // Close log file
    fileio_close(fstream, rcode);

    // Exit  -------------------------------------------------------------------------
    printf(PRT_MSG"\n");
    printf(PRT_MSG"Goodbye\n");
    return rcode;
}
