#include "drip.h"

/**
 * Open output stream to file, for writing data
 * @param path      Path to output file
 * @returns         File stream
 */
FILE* fileio_open(char* path) {
    FILE* file = fopen(path, "w+");

    if (file == NULL) {
        printf(PRT_ERROR"Could not open output file for writing \n");
        exit(EXIT_FAILURE);
    }

    return file;
}

/**
 * Write information about initial state of the system to a file
 * @param file      File stream pointer
 * @param params    Parameters of physical system
 * @param dmm       DeltaM method
 * @param x0m       x0 Method
 */
void fileio_writeParam(FILE* file, void* params) {
    double a  =  ((double *)params)[0];
    double R  =  ((double *)params)[1];
    double k  =  ((double *)params)[2];
    double b  =  ((double *)params)[3];
    double g  =  ((double *)params)[4];
    double xc =  ((double *)params)[5];
    double d  =  ((double *)params)[6];

    fprintf(file,"%.10e %.10e %.10e %.10e %.10e %.10e %.10e \n", 
                  a   , R   , k   , b   , g   , xc  , d );

    fflush(file);

}

/**
 * Write information about the current iteration to a file
 * @param file      File stream pointer
 * @param i         Iteration number
 * @param t         Current time
 * @param dt        Current timestep
 * @param x         Current position
 * @param v         Current velocity
 * @param m         Current mass
 * @param dripped   Did the tap drip this iteration?
 */
void fileio_writeIter(FILE* file, int i, double t, double dt, double x, double v, double m, bool dripped) {

    fprintf(file,"%.10d %.10e %.10e %.10e %.10e %.10e %d\n", 
                  i   , t   , dt  , x   , v   , m   , dripped);

}

/**
 * Write exit status and close file
 * @param file      File stream pointer
 * @param status    Exit status
 */
void fileio_close(FILE* file, int status) {

    fprintf(file,"%d \n", status);
    fflush(file);
    fclose(file);  

}