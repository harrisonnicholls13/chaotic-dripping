#include <stdio.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_odeiv2.h>

// https://www.gnu.org/software/gsl/doc/html/ode-initval.html

// Store the vector elements f_i(t,y,params) in the array dydt, for arguments (t, y) and parameters params.
int func (double t, const double y[], double f[], void *params) {
  (void)(t); /* avoid unused parameter warning */
  double mu = *(double *)params;
  f[0] = y[1];
  f[1] = -y[0] - mu*y[1]*(y[0]*y[0] - 1);
  return GSL_SUCCESS;
}

// Store the vector of partial derivative elements d/dt f_i(t,y,params) in the array dfdt.
// Store the Jacobian matrix J_{i,j} in the array dfdy.
int jac (double t, const double y[], double *dfdy,double dfdt[], void *params) {
  (void)(t); /* avoid unused parameter warning */
  double mu = *(double *)params;

  // A matrix view is a temporary object, stored on the stack, which can be used to operate on a subset of matrix elements
  gsl_matrix_view dfdy_mat = gsl_matrix_view_array (dfdy, 2, 2);
  gsl_matrix * m = &dfdy_mat.matrix;

  // Access matrix elements and set values of jacobian
  gsl_matrix_set (m, 0, 0, 0.0);
  gsl_matrix_set (m, 0, 1, 1.0);
  gsl_matrix_set (m, 1, 0, -2.0*mu*y[0]*y[1] - 1.0);
  gsl_matrix_set (m, 1, 1, -mu*(y[0]*y[0] - 1.0));

  // Vector of partial derivative elements
  dfdt[0] = 0.0;
  dfdt[1] = 0.0;
  return GSL_SUCCESS;
}

// Main function
int main () {
  // Parameter of system
  double mu = 10;

  // This data type defines a general ODE system with arbitrary parameters.
  // Takes in system of ODES, jacobian, dimensionality, and parameters
  gsl_odeiv2_system sys = {func, jac, 2, &mu};

  // The driver object is a high level wrapper that combines the evolution, control and stepper objects for easy use.
  // A pointer to a newly allocated instance of a driver object. 
  // Automatically allocates and initialises the evolve, control and stepper objects for ODE system sys using stepper type T. 
  // The initial step size is given in hstart.
  gsl_odeiv2_driver * d = gsl_odeiv2_driver_alloc_y_new (&sys, gsl_odeiv2_step_rk8pd, 1e-6, 1e-6, 0.0);
  
  // Initial conditions
  int i;
  double t = 0.0, t1 = 100.0;
  double y[2] = { 1.0, 0.0 };


  // Evolve system over time
  // https://www.gnu.org/software/gsl/doc/html/ode-initval.html#c.gsl_odeiv2_driver_apply
  for (i = 1; i <= 100; i++) {
      double ti = i * t1 / 100.0;

      // This function evolves the driver system d from t to t1. Initially vector y should contain the values of dependent variables at point t
      int status = gsl_odeiv2_driver_apply (d, &t, ti, y);

      if (status != GSL_SUCCESS) {
          printf ("error, return value=%d\n", status);
          break;
        }

      // Print state of system
      printf ("%.5e %.5e %.5e\n", t, y[0], y[1]);
    }

  // Tidy up
  gsl_odeiv2_driver_free (d);
  return 0;
}
