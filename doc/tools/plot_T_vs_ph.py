#!/usr/bin/env python3

# Python script to plot drip period T vs flow rate R
# Should generate a bifurcation map

# Import helper module
from drip_utils import *
import glob

# Read data
fpath = '/home/harrison/Documents/uni/Year 4/Computational Physics And Modelling/t2_project/'
logs = fpath + "./res/sweeps/" # Folder where logs are stored

plot_x = []
plot_y = []

num_points = 0

for f in glob.glob(logs + "R_*.dat"):
    params, data, exit_status = read_log(f)
    if (exit_status != 0):
        continue
    this_R = params["R"]
    for this_T in find_periods(data):
        plot_x.append(this_R)
        plot_y.append(this_T)
        num_points += 1

print("Total num of points   = %d"%num_points)

print("Plotting results...")

# Plot results
fig = plt.figure(figsize=(12,5))
ax = fig.add_subplot(111)
ax.set_title("Drip spectrum vs flow rate")

ax.scatter(plot_x,plot_y,color='black',s=1,marker=',')
ax.set_xlabel("Afflux")
ax.set_ylabel("Drip spectrum")

# ax.set_xscale("log")
# ax.invert_xaxis()

plt.subplots_adjust(bottom=0.18)
plt.tight_layout()
plt.show()
fig.savefig(fpath + "doc/figs/T_vs_ph.png",dpi=200)

print("Done!")


