#!/usr/bin/env python3

# Python script to plot drip period vs drip number

# Import helper module
from drip_utils import *
import scipy.signal as sig

# Read data
fpath = '/home/harrison/Documents/uni/Year 4/Computational Physics And Modelling/t2_project/'
params, data, exit_status = read_log(fpath + 'res/out.dat')
T = find_periods(data)

yf = np.fft.rfft(T)
xf = np.fft.rfftfreq(len(T), d=1)

fig,ax = plt.subplots(1,1,sharex=True,figsize=(10,6))

ax.set_title("RFFT of drip periods")

ax.plot(xf,yf,color='black',lw=1.5)
ax.set_ylabel("Amplitude")
ax.set_xlabel("Drip frequency [Hz] ")

plt.tight_layout()
fig.subplots_adjust(hspace=0.1)
plt.show()
fig.savefig(fpath + "doc/figs/fft.png",dpi=200)

print("Done!")