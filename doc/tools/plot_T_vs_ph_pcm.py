#!/usr/bin/env python3

# Python script to plot drip period T vs flow rate R
# Should generate a bifurcation map

# Import helper module
from drip_utils import *
import glob
import matplotlib.colors as mplc
from mpl_toolkits.axes_grid1 import make_axes_locatable
fpath = '/home/harrison/Documents/uni/Year 4/Computational Physics And Modelling/t2_project/'

# Set arguments
logs = fpath + "res/sweeps/" # Folder where logs are stored
prm = "R"

# Read data
print("Reading data...")
num_xs = 0
plot_x = []
plot_y = []
for f in glob.glob(logs + prm + "_*.dat"):
    params, data, exit_status = read_log(f)
    if (exit_status != 0):
        continue
    num_xs = num_xs + 1
    this_R = params[prm]
    for this_T in find_periods(data):
        plot_x.append(this_R)
        plot_y.append(this_T)

if (num_xs == 0):
    print("Error: No files found")
    exit()

ybins = 900
heatmap, xedges, yedges = np.histogram2d(plot_x, plot_y, bins=[ybins,num_xs])

print("Removing ones...")
shape = np.shape(heatmap)
for i in range(shape[0]):
    for j in range(shape[1]):
        if (heatmap[i][j] == 1):
            heatmap[i][j] = 0


print("Plotting results...")

# Plot results
fig = plt.figure(figsize=(14,6))
ax = fig.add_subplot(111)
ax.set_title("Drip spectrum vs flow rate")

hm_min = 0
hm_max = 10
norm = mplc.Normalize(vmin=hm_min, vmax=min(np.amax(heatmap),hm_max), clip=True)

im = ax.pcolorfast(xedges[:-1], yedges[:-1], heatmap.T, norm=norm, cmap="viridis")

divider = make_axes_locatable(ax)
cax = divider.append_axes('right', size='3%', pad=0.08)
fig.colorbar(im, cax=cax)

ax.set_xlabel("Flow rate [ml/s]")
ax.set_ylabel("Drip spectrum [s]")

plt.subplots_adjust(bottom=0.18)
plt.tight_layout()
fig.savefig(fpath + "doc/figs/T_vs_ph_pcm.png",dpi=200)
plt.show()

print("Done!")


