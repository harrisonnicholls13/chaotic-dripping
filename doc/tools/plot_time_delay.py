#!/usr/bin/env python3

# Python script to plot a time-delay diagram of T{n+1} vs T{n}

# Import helper module
from drip_utils import *

# Read data
fpath = '/home/harrison/Documents/uni/Year 4/Computational Physics And Modelling/t2_project/'
fname = fpath + "./res/out.dat" # Output data to use

plot_x = []
plot_y = []

params, data, exit_status = read_log(fname)
T_all = find_periods(data)
for i in range(len(T_all)-1):
    plot_x.append(T_all[i])
    plot_y.append(T_all[i+1])

print("Plotting results...")

# Plot results
fig = plt.figure(figsize=(8,6))
ax = fig.add_subplot(111)
ax.set_title("Time delay diagram")

ax.scatter(plot_x,plot_y,color='black',s=10)
ax.set_xlabel("Drip Period T_n   [s]")
ax.set_ylabel("Drip Period T_n+1 [s]")

fig.savefig(fpath + "doc/figs/time_delay.png",dpi=200)
plt.tight_layout()
plt.show()

print("Done!")


