#!/usr/bin/env python3

# Python script to plot residue variables vs time t

# Import helper module
from drip_utils import *

# Read data
fpath = '/home/harrison/Documents/uni/Year 4/Computational Physics And Modelling/t2_project/'
params, data, exit_status = read_log(fpath + 'res/out.dat')

data = np.array(data).T

t = data[1]
x = data[3]
v = data[4]
m = data[5]
p = [v[i]*m[i] for i in range(len(m))]

paramstr = ""
for key, value in params.items():
    if (key == "dm_mode"):
        break
    paramstr += "%s=%.4e  " % (key, value)

# Create figure
fig,(ax_x,ax_v,ax_m,ax_p) = plt.subplots(4,1,sharex=True,figsize=(11,8))
fig.set_facecolor("lightgrey")

ax_x.set_title("Residue coordinates")

# Plot x vs t
ax_x.plot(t,x,color='black',lw=1)
ax_x.set_ylabel("Position [cm]")
ax_x.invert_yaxis()
ax_x.axhline(y=params["xc"],color='red',lw=1.5)

# Plot v vs t
ax_v.plot(t,v,color='black',lw=1)
ax_v.set_ylabel("Velocity [cm/s]")

# Plot m vs t
ax_m.plot(t,m,color='black',lw=1)
ax_m.set_ylabel("Mass [g]")

# Plot p vs t
ax_p.plot(t,p,color='black',lw=1)
ax_p.set_ylabel("Momentum [g.cm/s]")
ax_p.set_xlabel("Time [s]")

# Put params on figure
plt.text(0.01, 0., paramstr, fontsize=10, color='black', ha='left', va='bottom', transform=fig.transFigure)

plt.tight_layout()
fig.subplots_adjust(hspace=0.2)

plt.show()
fig.savefig(fpath + "doc/figs/qs_vs_t.png",dpi=200)

print("Done!")