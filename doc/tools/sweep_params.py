#!/usr/bin/env python3

# Python script to run simulation for different input parameters

# Import helper module
from drip_utils import *
from threading import Thread
from time import sleep
import subprocess
import glob
import os
import progressbar


# Parameters
fpath = '/home/harrison/Documents/uni/Year 4/Computational Physics And Modelling/t2_project/'
binary = fpath + "./bin/drip" # Executable
logs = fpath + "./res/sweeps/" # Folder to put output logs in
tn_max = 15 # Max threads allowed to run at once
to_sweep = {
    "R": np.linspace(0.5,1.4,3000).tolist()
    # "xc": np.linspace(0.17, 0.36, 3500).tolist()
} # Dictionary of parameters to test (one at a time)


# Prepare to run threads
tracker = {} # Dict to track which params have been done
tn_cur = 0
nj_cur = 0 # Total number of jobs completed so far
nj_tot = 0 # Total number of jobs to do
nj_suc = 0 # Total number of jobs where solver was successful
threads = []
for k, v in to_sweep.items():
    # 0 => not done
    # 1 => active
    # 2 => done
    tracker[k] = [0 for _ in v]
    nj_tot += len(v)

print("Total number of jobs to be run: %d" % nj_tot)

# Remove old results
if not os.path.exists(logs):
    os.makedirs(logs)
for f in glob.glob(logs + "*.dat"):
    os.remove(f)

# Worker function - called once for each thread
def worker(wid):
    global tracker, nj_suc, nj_cur
    print("[Worker %2d] Started" % wid)

    # Find jobs in dictionary that aren't being done...
    # Loop over variables in dictionary
    for k,ja in tracker.items():
        # Loop over possible values variable could  take
        for ji,j in enumerate(ja):
            if (j == 0): # Has this value been run yet? (do code below if not)
                # Set up this job
                tracker[k][ji] = 1 # Mark job as active
                job_prm = str(k)
                job_val = str(to_sweep[k][ji])
                lf_name = logs + "./" + job_prm + "_" + str(ji) + ".dat"
                cmd = [binary,"fout",lf_name,job_prm,job_val]

                # Start subprocess (in current thread)
                sp = subprocess.Popen(cmd, stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)

                # End subprocess when done, and mark job as complete
                sp.wait()
                rc = sp.returncode
                nj_cur += 1
                if (rc == 0):
                    nj_suc += 1
                tracker[k][ji] = 2 # Mark job as done
               
    # Work done


# Dispatch threads
for i in range(tn_max):
    # Add thread to list of threads and start it
    threads.append(Thread(group=None, target=lambda:worker(i)))
    threads[i].start()
    tn_cur += 1
    sleep(0.1)

# Watch threads, and join each once they are done
bar = progressbar.ProgressBar(max_value=nj_tot)
while (tn_cur > 0):
    tn_cur = tn_max
    for i,t in enumerate(threads):
        if not t.is_alive():
            t.join()
            tn_cur -= 1
            # print("[Worker %2d] Ended"%i)
    bar.update(nj_cur)
    sleep(0.5)

print("\nAll jobs complete. Success rate = %g%%"%(nj_suc/nj_tot*100))

print("Done!")
