#!/usr/bin/env python3

# Python script to plot drip period vs drip number

# Import helper module
from drip_utils import *

# Read data
fpath = '/home/harrison/Documents/uni/Year 4/Computational Physics And Modelling/t2_project/'
params, data, exit_status = read_log(fpath + 'res/out.dat')
T = find_periods(data)

fig,ax = plt.subplots(1,1,sharex=True,figsize=(10,6))

ax.set_title("Drip period vs drip number")

ax.scatter(range(len(T)),T,color='black',s=4)
ax.set_ylabel("Drip Period [s]")
ax.set_xlabel("Drip number")

plt.tight_layout()
fig.subplots_adjust(hspace=0.1)
plt.show()
fig.savefig(fpath + "doc/figs/T_vs_N.png",dpi=200)

print("Done!")