# Helper module used by my plotting scripts

# Import modules
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.font_manager as mpfm
import sys

# Set custom aesthetic
fs = 12
mpl.rc('text',usetex=False)    
mpl.rc('font',family='sans-serif',size=fs)
prop = mpfm.FontProperties(size=fs)

def read_log(fdata):
    print("Reading '%s'... " % fdata,end='')
    sys.stdout.flush()
    # Read and process data
    file = open(fdata,'r')
    fdat = file.read().splitlines()
    file.close()

    fdat_header = fdat[0].split(" ")

    params = {}
    params["a"]   = float(fdat_header[0])
    params["R"]   = float(fdat_header[1])
    params["k"]   = float(fdat_header[2])
    params["b"]   = float(fdat_header[3])
    params["g"]   = float(fdat_header[4])
    params["xc"]  = float(fdat_header[5])
    params["d"]   = float(fdat_header[6])

    data = []
    for row in fdat[1:-1:]:
        row = row.split(" ")
        row[0] = int(  row[0]) # iter
        row[1] = float(row[1]) # t
        row[2] = float(row[2]) # dt
        row[3] = float(row[3]) # x
        row[4] = float(row[4]) # v
        row[5] = float(row[5]) # m
        row[6] = bool(int(row[6]) == 1) # dripped
        data.append(row)

    exit_status = int(fdat[-1])

    print("Done [%d]"%exit_status)
    
    return params, data, exit_status


def find_periods(data):
    T = []
    tl = -1.0
    for d in data:
        if (d[6] == True):
            # Dripped this iteration
            if (tl != -1.0):
                # Isn't the first drip
                T.append(abs(d[1]-tl))
            tl = d[1]
    return T