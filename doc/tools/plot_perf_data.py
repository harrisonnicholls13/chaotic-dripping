#!/usr/bin/env python3

# Script to plot performance data for report
# Compares execution time for different solver methods

from drip_utils import *

# Data
method  = ["RK8", "RK4", "RK2", "BDF"]
data_dy = np.array([[39.314 ,0.3998824827],[35.268,0.7410425089],[44.294 ,0.7029765288],[2.538,0.2544484231]]).T
data_fx = np.array([[521.124,1.395437566 ],[386.384,1.278212032],[210.708,0.6970968369],[1.77 ,0.2060339778]]).T

# Plot data

fig, ax1 = plt.subplots(figsize=(7,6))
ax2 = ax1.twinx()

ax1.errorbar(method,data_dy[0],yerr=data_dy[1]*5,color='blue',label="Dynamic timestep",lw=1,marker='x',elinewidth=1.5)
ax1.set_ylabel("Dynamic step: Execution time [s]")
ax1.yaxis.label.set_color('blue')
ax2.spines['left'].set_color('blue')

ax2.errorbar(method,data_fx[0],yerr=data_fx[1]*5,color='red',  label="Fixed timestep",lw=1,marker='x',elinewidth=1.5)
ax2.set_ylabel("Fixed step: Execution time [s]")
ax2.yaxis.label.set_color('red')
ax2.spines['right'].set_color('red')

plt.show()